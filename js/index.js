var xmlhttp = new XMLHttpRequest();
var url = "https://swapi.co/api/people/1/";

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        var name = document.getElementById('name').value = myArr.name;
        var height = document.getElementById('height').value = myArr.height;
        var mass = document.getElementById('mass').value = myArr.mass;
        var hair_color = document.getElementById('hair_color').value = myArr.hair_color;
        var skin_color = document.getElementById('skin_color').value = myArr.skin_color;
        var birth_year = document.getElementById('birth_year').value = myArr.birth_year;
        var gender = document.getElementById('gender').value = myArr.gender;
    }
};
xmlhttp.open("GET", url, true);
xmlhttp.send();
